import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {ClassifiedService} from './../classified.service'

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  constructor(public classifyService:ClassifyService,
              public imageService:ImageService,
              public ClassifiedService: ClassifiedService ) {}
  cat:string;
  

  ///////////////////////////////////////
  
 myFunc(cat:string){
  if (cat == 'business'){
  this.categoryImage = this.imageService.images[0];
  }
  else if (cat == 'entertainment'){
  this.categoryImage = this.imageService.images[1];
  }
  else if (cat == 'politic'){
 this.categoryImage = this.imageService.images[2];
 }
 else if (cat == 'sport'){
 this.categoryImage = this.imageService.images[3];
 }
 else if (cat == 'tech'){
   this.categoryImage = this.imageService.images[4];
   }
        this.ClassifiedService.addArticel(this.cat,this.classifyService.doc, this.categoryImage);
 // this.ans ="The data retention was successful"
}

//////////////////            



  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
        this.cat=this.category;
      }
    )
  }
}
