import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { ClassifiedService } from '../classified.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  constructor(public classifiedService:ClassifiedService) { }

  panelOpenState = false;
  //generals: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //generals: object[];
  articles$:Observable<any[]>;


  deleteArticle(id){
    this.classifiedService.deleteArticle(id);
  }

  ngOnInit() { 
    this.articles$ = this.classifiedService.getArticles(); 
  }
}
