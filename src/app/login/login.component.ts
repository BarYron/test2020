import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) { }

  email:string;
  password:string; 


  tempData$:Observable<Users|any>;
  errorMessage:string; 
  hasError:boolean = false; 

  onSubmit(){
  this.authService.login(this.email,this.password);
    
  }

  ngOnInit() {
    
  }


}
