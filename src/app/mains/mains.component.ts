import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {

  constructor(public mainService:MainService,
              private authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  userId:string;
  title:string;
  body:string;
  id:string;
  saveButton: string = 'save';


   //geting posts from json not from db
  main$: Main[];
  Comments$: Comments[];

 SavedPost(title:string , body:string){
     this.mainService.addPost(this.userId, title, body,0)
     this.saveButton='saved for later viewing'
    
  }

  ngOnInit() {
    this.mainService.getMains().subscribe(data => this.main$ = data)
    this.mainService.getComments().subscribe(data => this.Comments$ = data)

    ///////////////////////
   this.id = this.route.snapshot.params.id;
   this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
      //  if(this.id) {
        //  this.mainService.getMain(this.userId,this.id).subscribe(
          //  main => {
            //this.title = main.data().title;
            //this.body = main.data().body;
  //        })        
    //  }
   })
  }

}
