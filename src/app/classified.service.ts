import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {

  articlesCollection:AngularFirestoreCollection;


  constructor(private _http: HttpClient,private db:AngularFirestore, private router:Router) { }

  addArticel(category:string, body:string, image:string){
        const article = {category:category, body:body, image:image};
        this.db.collection('articles').add(article);
        this.router.navigate(['/articles']);
      }

  getArticles(): Observable<any[]> {
       return this.db.collection('articles').valueChanges({idField:'id'});
      } 

  deleteArticle(id:string){
        this.db.doc(`/articles/${id}`).delete();
      }
}
