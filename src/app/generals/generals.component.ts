import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralsService } from '../generals.service';
import { AuthService } from '../auth.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-generals',
  templateUrl: './generals.component.html',
  styleUrls: ['./generals.component.css']
})
export class GeneralsComponent implements OnInit {
  panelOpenState = false;
  //generals: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //generals: object[];
  posts$:Observable<any[]>;
  userId:string;
  

  addLikes(id,like){
    console.log(this.userId);
    this.generalservice.addlikes(this.userId,id,like);
  }

  deleteGeneral(id){
    this.generalservice.deleteGeneral(this.userId,id)
    console.log(id);
  }
  
  constructor(private generalservice:GeneralsService,
              public authService:AuthService,
              public datepipe: DatePipe) { }

  ngOnInit() { 
    console.log("NgOnInit started")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.generalservice.getGenerals(this.userId); 
      }
    )
  }
}