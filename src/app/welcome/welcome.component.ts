import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userId:string;
  userEmail;
   
  constructor( public authService:AuthService,
    ) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.userEmail =user.email;

      }
    )
  }

}
