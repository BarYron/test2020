import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Main } from './interfaces/main';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comments } from './interfaces/comments';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');

  constructor(private db: AngularFirestore,
    private authService:AuthService,
    private _http:HttpClient) { }

    

  apiURL='https://jsonplaceholder.typicode.com/posts/';
  apiComments = "https://jsonplaceholder.typicode.com/comments";

  getMains(){
   return this._http.get<Main[]>(this.apiURL);
}

getComments(){
  return this._http.get<Comments[]>(this.apiComments);
}


getMain(userId, id:string):Observable<any>{
  return this.db.doc(`users/${userId}/posts/${id}`).get();
}

addPost(userId:string, title:string, /*author:string*/body:string, like:number ){
  const post = {title:title,/*author:author,*/body:body, like:like}
  //this.db.collection('books').add(book)  
  this.userCollection.doc(userId).collection('posts').add(post);
}


}