// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
 firebaseConfig : {
    apiKey: "AIzaSyBFVMPAPZToq3qPjidd_P6HjsH3Qn8C59I",
    authDomain: "test20-bc5ac.firebaseapp.com",
    databaseURL: "https://test20-bc5ac.firebaseio.com",
    projectId: "test20-bc5ac",
    storageBucket: "test20-bc5ac.appspot.com",
    messagingSenderId: "231653334619",
    appId: "1:231653334619:web:e093d644b593b134fda97e",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
